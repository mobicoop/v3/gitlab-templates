#!/bin/bash

DEFAULT_V3_HOME=$(dirname $(dirname $(readlink -f $0)))
V3_HOME=${V3_HOME:-$DEFAULT_V3_HOME}

for repo in front/mobile api/gateway service/{ad,auth,configuration,geography,helpdesk,logger,notification,user} packages/{configuration,dddlibrary,health,message-broker}
do
  mkdir -p $V3_HOME/$repo/.gitlab/merge_request_templates
  cp -r .gitlab/merge_request_templates/* $V3_HOME/$repo/.gitlab/merge_request_templates/
done