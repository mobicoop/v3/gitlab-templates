#!/bin/env bash

git_dir_list=$(find .. -name .git -exec dirname {} \;)

for i in $git_dir_list
do

    cd "$i"
    if [ -d $i/.gitlab/merge_request_templates/ ]
    then
        echo "OK $i"
    else
        echo "KO $i"
    fi
    cd - > /dev/null
done
