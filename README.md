# Gitlab Templates

# Merge Request Templates

## Copy .gitlab/merge_request_templates in your git repository.

```
.gitlab/
└── merge_request_templates
    ├── default.md
    ├── release.md
    └── smallfix.md

```

git add,commit,push and merge...

## And now when you create a merge request, you will have the description to fill ;)

You can select the template with the menu on the left:

![Select A Template Model](/img/select_template.png "Select A Template Model").

- **default** is for "normal" feature, should be used most of time
- **smallfix** is a small version of default, for bugfix or very small change
- **release** is for a full release with change management
